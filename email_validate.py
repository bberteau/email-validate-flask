# -*- coding: utf8 -*-
import re
import unicodedata
from flask import Flask, request, render_template

APP = Flask(__name__)
APP.config.from_object(__name__)

# Load default config and overrid config from an environment variable
APP.config.update(dict(
    DEBUG=True,
    SECRET_KEY='iAw12-19Z0e2#Gf2@aM',
    USERNAME='admin',
    PASSWORD='default'
))

APP.config.from_envvar('FLASKR_SETTINGS', silent=True)


def is_valide_email(email):
    """
    Check email validity. Returns True if OK
    """
    # source http://sametmax.com/valider-une-adresse-email-avec-une-regex-en-python/
    email_re = re.compile(r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"
                          r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"'
                          r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', re.IGNORECASE)
    return email_re.search(email)


def _convert(listemails, alpha=True, lignes=True):
    """
    Delete bad caracters from pasted emails
    """
    # Caractères à remplacer dans le bloc
    caracteres = [";", ",", " ", "/", "\t", "\r", "\"", "\'", "\\",
                  "<", ">", ")", "(", ":", ""]
    listemails = listemails.lower()
    # Suppression des caractères indésirables
    for car in caracteres:
        listemails = listemails.replace(car, '\n')
    listemails = listemails.replace(" ", "")
    # listemails = listemails.encode('ascii', 'ignore')
    listemails = set(listemails.split("\n"))
    listemails = [unicodedata.normalize('NFKD', mail).encode('ascii', 'ignore')
                  for mail in listemails]
    listemails = list(listemails)
    listevalides = []

    # Vérification des adresses mails
    listevalides = [mail.strip() for mail in listemails if "@" in mail and is_valide_email(mail)]
    if alpha:
        listevalides = sorted(listevalides)
    if lignes:
        listevalides = "\n".join(listevalides)
    else:
        listevalides = ";".join(listevalides)

    listeinvalides = [mail for mail in listemails if (not is_valide_email(mail) and mail)]
    listeinvalides = sorted(listeinvalides)
    return listevalides, listeinvalides


@APP.route('/index')
@APP.route('/', methods=["GET", "POST"])
def index():
    """
    Default route
    """
    alpha = True
    lignes = True
    averifier = ""
    listeinvalides = []
    listevalides = []
    if request.method == 'POST':
        averifier = request.form['averifier']
        alpha = 'alpha' in request.form
        lignes = 'lignes' in request.form
        listevalides, listeinvalides = _convert(averifier, alpha=alpha, lignes=lignes)

    mails = {'averifier': averifier,
             'valides': listevalides, 'invalides': listeinvalides,
             'alpha': alpha, 'lignes': lignes}

    return render_template('index.html', mails=mails)


if __name__ == '__main__':
    APP.run(host='0.0.0.0', port=8001, debug=True)
